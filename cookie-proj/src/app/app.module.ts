import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CountComponent } from './count/count.component';
import { BakeComponent } from './bake/bake.component';
import { HttpClientModule } from '@angular/common/http';
import { EventsSidebarComponent } from './events-sidebar/events-sidebar.component';

@NgModule({
  declarations: [
    AppComponent,
    CountComponent,
    BakeComponent,
    EventsSidebarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
