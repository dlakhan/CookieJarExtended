import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BakeComponent } from './bake/bake.component';
import { CountComponent } from './count/count.component';
const routes: Routes = [
  {
    path: 'bake',
    component: BakeComponent
  },
  {
      path: 'count',
      component: CountComponent
   },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
