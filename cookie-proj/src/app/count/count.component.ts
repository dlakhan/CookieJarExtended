import { Component, OnInit, OnDestroy } from '@angular/core';
import { SawtoothService } from '../sawtooth.service';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-count',
  templateUrl: './count.component.html',
  styleUrls: ['./count.component.scss']
})
export class CountComponent implements OnInit {
  public cookieCount: any;

  constructor(private Data : SawtoothService, private router :RouterModule) {
   }

   ngOnInit() {
    this.Data.sendToRestAPI(null)
    .then((count) => {
        console.log(count);
        alert("Cookie count: " + (count ? count : 0));
        this.cookieCount = count ? count : 0;
      })
      .catch((error) => {
        console.error(error);
      }); 
    }
        
          
     
  countCookie(event){
    event.preventDefault();
    this.Data.sendToRestAPI(null)
    .then((count) => {
      this.cookieCount = count ? count : 0;
    })
    console.log(this.cookieCount);
    
    // this.Data.getUserDetails(username)
  }
}
