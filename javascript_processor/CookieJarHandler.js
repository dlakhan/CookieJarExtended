'use strict'

// TO-DO
// Require the TransactionHandler module from SDK
// Extend the TransactionHandler module to create custom handler
// Register the transaction family, version and namespaces in the constructor
// Implement the apply function:
//  1 - Decode the transaction payload
//      Cookiejar payload is of the form "<action>,<quantity>"
//  2 - Check the cookie action to perform from the payload and pass to handler
//  3 - For bake, insert/update state
//  4 - For eat, first check if there are enough cookies, then update state.
//      If not enough cookies are present, throw InvalidTransactionError
//  5 - Getting and setting to states should be 'utf8' encoded (use the 'encoder', 'decoder' below)
// Use the _hash function for address creation


const {
  InvalidTransaction,
  InternalError
} = require('sawtooth-sdk/processor/exceptions')
const crypto = require('crypto')
const { TextEncoder, TextDecoder } = require('text-encoding/lib/encoding')
const protobuf = require("protobufjs")
const _hash = (x) => crypto.createHash('sha512').update(x).digest('hex').toLowerCase().substring(0, 64)
var encoder = new TextEncoder('utf8')
var decoder = new TextDecoder('utf8')


const { TransactionHandler } = require('sawtooth-sdk/processor/handler')// require the transaction module here from SDK

//declare all CONSTANTS 
const FAMILY_NAME = 'cookiejar';
const NAMESPACE = _hash(FAMILY_NAME).substr(0,6);

var cookieBake;
protobuf.load("CookieStructure.proto", function(err,root) 
  {
    if (err)
    {
      throw (err);
    }
    cookieBake = root.lookupType("Cookiestructure.cookieJar.cookieType");
    
  });


//function to return decoded  payload <action,quantity>

const _decodeRequest = function(payload)
{
  var payloadActions = payload.split(',');
  var payloadDecoded = {action: payloadActions[0], cType: payloadActions[1], quantity: Number(payloadActions[2])};
  return payloadDecoded;
}

//check payload for null i.e. if it follows <action,quantity>

const checkPayload = function(payloadDecoded)
{
  if(payloadDecoded.action && payloadDecoded.cType && payloadDecoded.quantity)
  {
    return true;
  }
  return false ;
}



//function to set entry into the state using 'setState' method
const setEntry = function(context, stateAddress, stateValue)
{
  let encodedStateValue;
  var keyPair = stateValue;
  console.log("keyPair from bakeCookies: ", keyPair)
  

    encodedStateValue = cookieBake.encode(
    {
      cookieType: Object.keys(keyPair)[0],
      cookieCount: Number(keyPair[Object.keys(keyPair)[0]])
    }
    ).finish();


    console.log("Encoded Value: ", encodedStateValue);

    var decodedValue = cookieBake.decode(encodedStateValue);

    console.log("Decoded Value: ", decodedValue.cookieCount);

    let addressValuePairs = {[stateAddress] : encodedStateValue };
    console.log("AddressValuePair: ", addressValuePairs);
    return (context.setState(addressValuePairs))

}

const decodeProto = function(stateValue)
{
    var buffer = cookieBake.decode(stateValue);

    return buffer;
}



//baking cookies - this fucntion will take user input and update the state
const bakeCookies = function(context, stateAddress, cType, quantity)
{
  let currentValue = {}
  let count
  let buffItem
  let getPromise = context.getState([stateAddress]);
  return getPromise.then(function getCount(stateMappings){
    console.log("****INSIDE GETCURRENTVALUE****")
    console.log("cType from apply:", cType)
    
    let stateValue = stateMappings[stateAddress];

    console.log("getstateVal= ", stateValue, typeof(stateValue))
    var buffer = decodeProto(stateValue);
    console.log("cookieBake BUFFER: ", buffer)
   
    if (buffer === undefined)
    {
      console.log("inside NULL BLOCK--count: ", buffer, typeof(buffer))
      currentValue[cType] = quantity;
      
      console.log('Cookies send for baking: ', currentValue[cType])
      return (setEntry(context,stateAddress,currentValue));
    }
    else
    {
      for (buffItem in buffer)
      {
        if (buffItem.cookieType === cType)
        {
          count = buffer.cookieCount
          let newCount = quantity + count;
          currentValue[cType] = newCount;
          console.log("NEW COUNT: ", newCount , typeof(newCount))
          console.log('Cookies send for baking :'+ newCount)
          return (setEntry(context,stateAddress,currentValue));
          
        }
        
      }
          count = buffer.cookieCount
          let newCount = quantity + count;
          currentValue[cType] = newCount;
          console.log("NEW COUNT: ", newCount , typeof(newCount))
          console.log('Cookies send for baking :'+ newCount)
          return (setEntry(context,stateAddress,currentValue));


      
      
    }

  });
}


//eating cookies- this will take user input and decrement from state value if possible
const eatCookies = function (context,stateAddress,quantity)
{
  //var currentCount = getCurrentValue(context, stateAddress);
  let currentCount = 0
  let getPromise = context.getState([stateAddress]);
  return getPromise.then(function getCount(stateMappings){
    let count = 0 
    let stateValue = stateMappings[stateAddress];
    console.log("getstateVal= ", stateValue, typeof(stateValue))
    count = decoder.decode(stateValue);
    
    //to check if the count is null (NaN)
    if(isNaN(count) || Number(count) < quantity )
    {
      throw InternalError('Not enough cookies to Eat !');
    }
    else
    {
      currentCount = Number(count);
      let newCount = (currentCount - quantity).toString();
      console.log('Cookies remaining :'+newCount)
      return (setEntry(context,stateAddress,newCount));
    }

  });


}

//simple list to handle action in any text format
const bakeList = ['Bake','bake','BAKE'];
const eatList = ['Eat','eat','EAT'];


//main class that extends the TransactionHandler class
class CookieJarHandler extends TransactionHandler
{
  
  
  constructor()
  {
    super(FAMILY_NAME,['1.0'],[NAMESPACE])
  }

  //the main apply function where all execution happens
  apply(txProcessRequest, context)
  {
    //extracting and decoding payload from TransactionProcessRequest
    var payload = txProcessRequest.payload;
    var payloadDecoded = _decodeRequest(payload.toString());

    console.log("TXPROCREQUEST: ",txProcessRequest)
    console.log("Payload Actions: ",payloadDecoded.action, typeof (payloadDecoded.action))
    console.log("Payload Type: ",payloadDecoded.cType)
    console.log("Payload Quantity: ",payloadDecoded.quantity, typeof (payloadDecoded.quantity))
    console.log("PAYLOAD: ",payload.toString()) 

    //getting state address from TransactionHeader
    let txHeader = txProcessRequest.header;
    let userPublicKey = txHeader.signerPublicKey;
    let stateAddress = NAMESPACE+_hash(userPublicKey)
    


    //executing the actions (bake/eat/count) from client side
    if (checkPayload(payloadDecoded))
    {

      if ( bakeList.indexOf(payloadDecoded.action) > -1)
      {
        console.log("***HITING BAKE BLOCK NEW***")
        return (bakeCookies(context,stateAddress,payloadDecoded.cType,payloadDecoded.quantity));

      }
      else if (eatList.indexOf(payloadDecoded.action) > -1)
      {
        console.log("***HITING EAT BLOCK***")
        return (eatCookies(context,stateAddress,payloadDecoded.quantity));
      }
      else
   
      {
        throw new InvalidTransaction(`Action must be bake or eat `)
      }
    }



   
  }
}



module.exports = CookieJarHandler// Module name here